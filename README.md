# switch1

A small pointless switch breakout to practice [Kicad](https://kicad.org) layout
and component creation.

![Board render](switch1.jpg)

## License

There's nothing really worthwhile in this project, so I retain all rights.

